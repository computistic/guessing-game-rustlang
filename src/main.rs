extern crate rand;
extern crate colored;

use std::cmp::Ordering;
use rand::Rng;
use colored::*;

/**
  * main.rs
  * A guessing game built as part of learning Rust lang
  */

fn main() {
    let mut rng = rand::thread_rng();
    let secret_number = rng.gen_range(1, 101);
    
    println!("\n\t\t{}\n", "Welcome".bold().green());
    println!("\t\t{} by {}\n", "Guessing Game".bold().blue(), "Hannan Ali".underline().red());

    loop {
        // Variables are mutable by default `mut` keyword is used to declare variables as mutable
        let mut guess = String::new();


        println!("Please input your number: ");

        std::io::stdin()
            .read_line(&mut guess)
            .expect("Failed to process your guess");

        // Converting guess with string type to u32
        let guess : u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(num) => {
                println!("{}{}", "Please enter a ".yellow(), "number".red().bold());
                continue;
            },
        };

        println!("\n\n{}", "Result".underline().magenta().bold());
        
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too Small"),
            Ordering::Greater => println!("Too High"),
            Ordering::Equal => {
                println!("{}", "Congratulations!".cyan().bold());
                println!("{}", "You win the game!".green().underline().bold().italic());
                break;
            },
        }

        println!("\n");
    }
}
